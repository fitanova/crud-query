@extends('layout.master')

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title text-bold">Tabel Data Pemain</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success')}}
                    </div>
                  @endif
                  <a class="btn btn-primary" href="/cast/create">Daftar Pemain Baru</a>
                <table class="table table-bordered table-striped mt-3">
                  <thead>                  
                    <tr class="text-center">
                      <th style="width: 10px">No</th>
                      <th style="width: 375px">Nama</th>
                      <th style="width: 75px">Umur</th>
                      <th>Bio</th>
                      <th style="width: 215px">Informasi</th>
                      <th style="width: 100px">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($cast as $key => $cast)
                        <tr>
                            <td>
                                {{$key +1}}
                            </td>
                            <td>
                                {{$cast->nama}}
                            </td>
                            <td class="text-center">
                                {{$cast->umur}}
                            </td>
                            <td>
                                {{$cast->bio}}
                            </td>
                            <td style="display:flex">
                                <a href="/cast/{{$cast->id}}" class="btn btn-info">Show More</a>
                                <a href="/cast/{{$cast->id}}/edit" class="btn btn-default">Edit Data</a>
                            </td>
                            <td class="text-center">
                                <form action="/cast/{{$cast->id}}" method="post">
                                    @csrf 
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                    
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td>Tidak ada Daftar Pemain</td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
@endsection

